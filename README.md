# Welcome to My Spaces

## Overview

##### My Spaces is a Vue app where I have all the space I need to share my thoughts with the world.

## Highlights

##### Before this project I had never worked with RAML, but now I am a huge fan. I am glad that I was able to learn about it and all the potential benefits that it provides. I will definitely start using it in my personal side projects. I'm not sure if this was part of the test, but there was a misplaced comma in the RAML file that took some digging to find so the mock API would work.

## Lowlights

##### I was not able to get all of the functionality into the app that I would have liked. I would also like to add some more testing (the tests are not passing on the frontend). I am a big proponent of testing because good testing is a developers best friend. I also would like to clean up some of the code in some areas that it is not very DRY, make the app more compatible with smaller screens, and rework the overall design.

## How To Get Started

```
clone repository: git clone https://masonjterry@bitbucket.org/masonjterry/ui-coding-exercise.git
```

#### Frontend

```
cd frontend
npm install
npm run frontend
```

#### Backend

```
cd backend
npm install
npm run backend
```

#### Testing Frontend

```
cd frontend
npm run test
```

#### Testing Backend

```
cd backend
npm run test
```
