const mockService = require('osprey-mock-service')
const express = require('express')
const cors = require('cors')
const path = require('path')
const morgan = require('morgan')
const app = express()
const PORT = 4000 || process.env.PORT

mockService.loadFile(path.join(__dirname, 'space-api.raml')).then(mockApp => {
  app.use(morgan('dev'))
  app.use(cors())
  app.use(mockApp)
  app.listen(PORT, () => console.log(`Listening on port ${PORT}`))
})
