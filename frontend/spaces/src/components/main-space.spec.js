import { shallowMount, createdLocalVue } from '@vue/test-utils'
import MainSpace from './main-space'

const localVue = createdLocalVue()

describe('main-space component', () => {
  it('should display heading and body', () => {
    const mainSpaceComponent = shallowMount(MainSpace, {
      propsData: { dispaly: 'My First Space' },
      localVue
    })
    const mainSpaceText = mainSpaceComponent.text()
    expect(mainSpaceComponent).toContain('My First Space')
  })
})
