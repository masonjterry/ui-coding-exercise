import Api from './Api'

export default {
  getSpaces() {
    return Api().get('/space')
  },
  getSpace(spaceId) {
    return Api().get(`/space/${spaceId}`)
  },
  getEntries(spaceId) {
    return Api().get(`space/${spaceId}/entries`)
  },
  getAssets(spaceId) {
    return Api().get(`/space/${spaceId}/assets`)
  },
  getEntry(spaceId, entryId) {
    return Api().get(`/space/${spaceId}/entries/${entryId}`)
  },
  getAsset(spaceId, assetId) {
    return Api().get(`/space/${spaceId}/assets/${assetId}`)
  }
}
